terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

variable SSM_KEY {
  type = string
}


# Create a ssm activation key
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "test_role" {
  name               = "test_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "test_attach" {
  role       = aws_iam_role.test_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_ssm_activation" "foo" {
  count = var.SSM_KEY == "Jeffin_key" ? 1 : 0
  name               = "test_ssm_activation"
  description        = "Test"
  iam_role           = aws_iam_role.test_role.id
  registration_limit = "5"
  depends_on         = [aws_iam_role_policy_attachment.test_attach]
  tags = {
    name = "key1"
  }
}

# resource "local_file" "activation_code" {
#     content  = aws_ssm_activation.foo.activation_code
#     filename = "activation_code.txt"
# }

output "key_value" {
  value = var.SSM_KEY
}

# output "activation_id" {
# value       =  aws_ssm_activation.foo.id
# }

# output "activation_code" {
#   value       =  aws_ssm_activation.foo.activation_code
# }
